//
//  ViewController.swift
//  Source control
//
//  Created by Isaac on 2020-09-02.
//  Copyright © 2020 Rander. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    /**
     Adds 2 numbers together and returns result. Modified
     - parameter num1: The first number
     - parameter num2: the second number
     - returns: the sum of num1 and num2
     */
    func addNumbers (num1: Int, num2: Int) -> Int {
        return num1 + num2
    }

}

